package mx.com.smartmind.colorhack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Password extends Code {



    public Password(ArrayList<ColorValue> colors) {
        super(colors);
    }

    public static Password createPassword() {
        ArrayList<ColorValue> colors = new ArrayList<>();
        Random random = new Random();
        colors.add(ColorValue.values()[random.nextInt(7)]);
        colors.add(ColorValue.values()[random.nextInt(7)]);
        colors.add(ColorValue.values()[random.nextInt(7)]);
        colors.add(ColorValue.values()[random.nextInt(7)]);
        return new Password(colors);
    }

    public ArrayList<Status> checkAttempt(Attempt attempt) {
        ArrayList<ColorValue> temporalPassword = new ArrayList<>();
        for(int i = 0; i < 4; i++) {
            temporalPassword.add(colors.get(i));
        }
        ArrayList<ColorValue> temporalAttempt = new ArrayList<>();
        for(int i = 0; i < 4; i++) {
            temporalAttempt.add(attempt.getColors().get(i));
        }
        ArrayList<Status> checks = new ArrayList<>();
        checks.add(Status.Fail);
        checks.add(Status.Fail);
        checks.add(Status.Fail);
        checks.add(Status.Fail);

        int index = 0;
        for(int i = 3; i >= 0; i--) {
            if(temporalAttempt.get(i) == temporalPassword.get(i)) {
                checks.set(index, Status.Perfect);
                temporalAttempt.remove(i);
                temporalPassword.remove(i);
                index++;
            }
        }
        for(int i = 0; i < temporalAttempt.size(); i++) {
            for(int j = 0; j < temporalPassword.size(); j++) {
                if(temporalAttempt.get(i) == temporalPassword.get(j)) {
                    checks.set(index, Status.Good);
                    temporalPassword.remove(j);
                    index++;
                    break;
                }
            }
        }
        Collections.shuffle(checks);
        return checks;
    }
}
