package mx.com.smartmind.colorhack;

public enum Status {
    Perfect,
    Good,
    Fail
}
