package mx.com.smartmind.colorhack;

public class Utils {
    public static int Color2Hex(ColorValue color) {
        int colorValue = R.color.colorBase;
        switch (color) {
            case Blue:
                colorValue = R.color.blue;
                break;
            case Red:
                colorValue = R.color.red;
                break;
            case Yellow:
                colorValue = R.color.yellow;
                break;
            case Green:
                colorValue = R.color.green;
                break;
            case Gray:
                colorValue = R.color.gray;
                break;
            case Black:
                colorValue = R.color.black;
                break;
            case None:
                colorValue = R.color.colorBase;
                break;
        }
        return colorValue;
    }
}
