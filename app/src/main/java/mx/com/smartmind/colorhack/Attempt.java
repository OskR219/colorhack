package mx.com.smartmind.colorhack;

import java.util.ArrayList;

public class Attempt extends Code {

    private boolean alreadyChecked = false;

    public boolean isAlreadyChecked() {
        return alreadyChecked;
    }

    public void setAlreadyChecked(boolean alreadyChecked) {
        this.alreadyChecked = alreadyChecked;
    }

    public Attempt(ArrayList<ColorValue> colors) {
        super(colors);
    }

}
