package mx.com.smartmind.colorhack;

public enum ColorValue {
    Blue,
    Red,
    Yellow,
    Green,
    Gray,
    Black,
    None
}
