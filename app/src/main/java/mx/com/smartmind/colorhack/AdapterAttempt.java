package mx.com.smartmind.colorhack;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

public class AdapterAttempt extends ArrayAdapter<Attempt> {

    private Context context;
    private ArrayList<Attempt> attempts;

    private Password password;

    public AdapterAttempt(@NonNull Context context, @NonNull ArrayList<Attempt> attempts, Password password) {
        super(context, 0, attempts);
        this.context = context;
        this.attempts = attempts;
        this.password = password;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(context).inflate(R.layout.adapter_attempts, parent,false);


        Attempt currentAttempt = attempts.get(position);
        ArrayList<ColorValue> colors = currentAttempt.getColors();
        ImageView password_1 = listItem.findViewById(R.id.password_1);
        int color_1 = Utils.Color2Hex(colors.get(0));
        password_1.setColorFilter(ContextCompat.getColor(context, color_1), android.graphics.PorterDuff.Mode.SRC_IN);

        ImageView password_2 = listItem.findViewById(R.id.password_2);
        int color_2 = Utils.Color2Hex(colors.get(1));
        password_2.setColorFilter(ContextCompat.getColor(context, color_2), android.graphics.PorterDuff.Mode.SRC_IN);

        ImageView password_3 = listItem.findViewById(R.id.password_3);
        int color_3 = Utils.Color2Hex(colors.get(2));
        password_3.setColorFilter(ContextCompat.getColor(context, color_3), android.graphics.PorterDuff.Mode.SRC_IN);

        ImageView password_4 = listItem.findViewById(R.id.password_4);
        int color_4 = Utils.Color2Hex(colors.get(3));
        password_4.setColorFilter(ContextCompat.getColor(context, color_4), android.graphics.PorterDuff.Mode.SRC_IN);

        if(!currentAttempt.isAlreadyChecked()) {
            ArrayList<Status> checks = password.checkAttempt(currentAttempt);

            ImageView check_1 = listItem.findViewById(R.id.check_1);
            ImageView check_2 = listItem.findViewById(R.id.check_2);
            ImageView check_3 = listItem.findViewById(R.id.check_3);
            ImageView check_4 = listItem.findViewById(R.id.check_4);
            ImageView currentCheck;

            currentCheck = check_1;
            switch (checks.get(0)) {
                case Good:
                    currentCheck.setColorFilter(ContextCompat.getColor(context, R.color.purple), android.graphics.PorterDuff.Mode.SRC_IN);
                    break;
                case Perfect:
                    currentCheck.setColorFilter(ContextCompat.getColor(context, R.color.orange), android.graphics.PorterDuff.Mode.SRC_IN);
                    break;
            }
            currentCheck = check_2;
            switch (checks.get(1)) {
                case Good:
                    currentCheck.setColorFilter(ContextCompat.getColor(context, R.color.purple), android.graphics.PorterDuff.Mode.SRC_IN);
                    break;
                case Perfect:
                    currentCheck.setColorFilter(ContextCompat.getColor(context, R.color.orange), android.graphics.PorterDuff.Mode.SRC_IN);
                    break;
            }
            currentCheck = check_3;
            switch (checks.get(2)) {
                case Good:
                    currentCheck.setColorFilter(ContextCompat.getColor(context, R.color.purple), android.graphics.PorterDuff.Mode.SRC_IN);
                    break;
                case Perfect:
                    currentCheck.setColorFilter(ContextCompat.getColor(context, R.color.orange), android.graphics.PorterDuff.Mode.SRC_IN);
                    break;
            }
            currentCheck = check_4;
            switch (checks.get(3)) {
                case Good:
                    currentCheck.setColorFilter(ContextCompat.getColor(context, R.color.purple), android.graphics.PorterDuff.Mode.SRC_IN);
                    break;
                case Perfect:
                    currentCheck.setColorFilter(ContextCompat.getColor(context, R.color.orange), android.graphics.PorterDuff.Mode.SRC_IN);
                    break;
            }
            currentAttempt.setAlreadyChecked(true);
        }
        return listItem;
    }
}
