package mx.com.smartmind.colorhack;

import java.util.ArrayList;

public class Code {

    protected ArrayList<ColorValue> colors;

    public Code(ArrayList<ColorValue> colors) {
        this.colors = colors;
    }

    public ArrayList<ColorValue> getColors() {
        return colors;
    }
}
