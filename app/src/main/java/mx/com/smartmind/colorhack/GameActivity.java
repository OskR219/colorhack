package mx.com.smartmind.colorhack;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

public class GameActivity extends AppCompatActivity {

    private Context context;

    private ArrayList<Attempt> attempts;
    private ListView listAttempts;
    private AdapterAttempt adapterAttempt;

    private ImageButton color_1;
    private int currentColorValue_1 = 6;

    private ImageButton color_2;
    private int currentColorValue_2 = 6;

    private ImageButton color_3;
    private int currentColorValue_3 = 6;

    private ImageButton color_4;
    private int currentColorValue_4 = 6;

    private Button btnHack;

    private Password password;

    private ImageView password_1;
    private ImageView password_2;
    private ImageView password_3;
    private ImageView password_4;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        listAttempts = findViewById(R.id.listAttempts);

        btnHack = findViewById(R.id.hack_btn);

        color_1 = findViewById(R.id.color_1);
        color_2 = findViewById(R.id.color_2);
        color_3 = findViewById(R.id.color_3);
        color_4 = findViewById(R.id.color_4);

        password_1 = findViewById(R.id.password_1);
        password_2 = findViewById(R.id.password_2);
        password_3 = findViewById(R.id.password_3);
        password_4 = findViewById(R.id.password_4);

        newGame();

        attempts = new ArrayList<>();
        adapterAttempt = new AdapterAttempt(this, attempts, password);
        listAttempts.setAdapter(adapterAttempt);

        btnHack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<ColorValue> colors = new ArrayList<>();
                colors.add(ColorValue.values()[currentColorValue_1]);
                colors.add(ColorValue.values()[currentColorValue_2]);
                colors.add(ColorValue.values()[currentColorValue_3]);
                colors.add(ColorValue.values()[currentColorValue_4]);
                attempts.add(new Attempt(colors));
                adapterAttempt.notifyDataSetChanged();
            }
        });


        color_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentColorValue_1 = currentColorValue_1 >= 6 ? 0 : currentColorValue_1 + 1;
                int color = Utils.Color2Hex(ColorValue.values()[currentColorValue_1]);
                color_1.setColorFilter(ContextCompat.getColor(context, color), android.graphics.PorterDuff.Mode.SRC_IN);
            }
        });


        color_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentColorValue_2 = currentColorValue_2 >= 6 ? 0 : currentColorValue_2 + 1;
                int color = Utils.Color2Hex(ColorValue.values()[currentColorValue_2]);
                color_2.setColorFilter(ContextCompat.getColor(context, color), android.graphics.PorterDuff.Mode.SRC_IN);
            }
        });


        color_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentColorValue_3 = currentColorValue_3 >= 6 ? 0 : currentColorValue_3 + 1;
                int color = Utils.Color2Hex(ColorValue.values()[currentColorValue_3]);
                color_3.setColorFilter(ContextCompat.getColor(context, color), android.graphics.PorterDuff.Mode.SRC_IN);
            }
        });


        color_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentColorValue_4 = currentColorValue_4 >= 6 ? 0 : currentColorValue_4 + 1;
                int color = Utils.Color2Hex(ColorValue.values()[currentColorValue_4]);
                color_4.setColorFilter(ContextCompat.getColor(context, color), android.graphics.PorterDuff.Mode.SRC_IN);
            }
        });

    }

    private void newGame() {
        password = Password.createPassword();
        int colorPassword;
        colorPassword = Utils.Color2Hex(password.getColors().get(0));
        password_1.setColorFilter(ContextCompat.getColor(context, colorPassword), android.graphics.PorterDuff.Mode.SRC_IN);
        colorPassword = Utils.Color2Hex(password.getColors().get(1));
        password_2.setColorFilter(ContextCompat.getColor(context, colorPassword), android.graphics.PorterDuff.Mode.SRC_IN);
        colorPassword = Utils.Color2Hex(password.getColors().get(2));
        password_3.setColorFilter(ContextCompat.getColor(context, colorPassword), android.graphics.PorterDuff.Mode.SRC_IN);
        colorPassword = Utils.Color2Hex(password.getColors().get(3));
        password_4.setColorFilter(ContextCompat.getColor(context, colorPassword), android.graphics.PorterDuff.Mode.SRC_IN);
        currentColorValue_1 = 6;
        currentColorValue_2 = 6;
        currentColorValue_3 = 6;
        currentColorValue_4 = 6;
        int color = Utils.Color2Hex(ColorValue.values()[6]);
        color_1.setColorFilter(ContextCompat.getColor(context, color), android.graphics.PorterDuff.Mode.SRC_IN);
        color_2.setColorFilter(ContextCompat.getColor(context, color), android.graphics.PorterDuff.Mode.SRC_IN);
        color_3.setColorFilter(ContextCompat.getColor(context, color), android.graphics.PorterDuff.Mode.SRC_IN);
        color_4.setColorFilter(ContextCompat.getColor(context, color), android.graphics.PorterDuff.Mode.SRC_IN);
    }
}
